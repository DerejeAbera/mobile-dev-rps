import "react-native-gesture-handler";
import React, {useEffect, useState} from 'react';
import { StatusBar } from 'expo-status-bar';
import {Platform, StyleSheet} from 'react-native';
import {TokenIdContext, GameContext, GameListContext} from "./components/Contexts";
import { NavigationContainer } from '@react-navigation/native';
import Nav from "./components/Nav";
import {ActivityIndicator} from "react-native-web";
import {navigationRef} from './components/RootNavigation'

import RpsClientApi from "./components/RpsClientApi";
import Footer from "./components/Footer";


export default function App() {
  const [dataLoading, finishLoading] = useState(true);
  const [tokenId, setTokenId] = useState('');
  const [game, setGame] = useState(null);
  const [gameList, setGameList] = useState([]);

  useEffect(() => {
    RpsClientApi.getNewTokenId(setTokenId)
        .catch((error) => console.error(error))
        .finally(() => finishLoading(false));
    console.log("token Id"+ tokenId)
  }, []);

  useEffect(() => {
        setInterval(() => {
            RpsClientApi.getGameStatus(tokenId, setGame).catch(err=>console.log(err));
        }, 10000)
  }, [tokenId, game, setGame]);

  useEffect(() => {
        setInterval(() => {
            RpsClientApi.openGames(tokenId, setGameList).catch(err=>console.log(err));
        }, 10000)
  }, [tokenId, setGameList]);

  return (
      <TokenIdContext.Provider value={tokenId}>
       <GameContext.Provider value={[game, setGame]}>
         <GameListContext.Provider value={[gameList, setGameList]}>
             <NavigationContainer
            style={{ paddingTop: Platform.OS === 'android' ? StatusBar.currentHeight : 0}}
            ref={navigationRef}
             >
          {dataLoading? <ActivityIndicator/> : (
              <Nav/>
          )}
          <Footer/>
        </NavigationContainer>
       </GameListContext.Provider>
       </GameContext.Provider>
      </TokenIdContext.Provider>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
