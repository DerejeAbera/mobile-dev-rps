import { createContext } from 'react';

export const GameListContext = createContext('');

export const GameContext = createContext('');

export const TokenIdContext = createContext('');

