import React from 'react';
import { StyleSheet, View } from 'react-native';
import Text from "react-native-web/dist/vendor/react-native/Animated/components/AnimatedText";

const Footer = () =>{
    return (
        <View style={styles.footer}>
            <Text> RPS@EC.UTBD </Text>
        </View>
    );
}
const styles=StyleSheet.create({
    footer:{
        width:'100%',
        height:75,
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center',
    },
    button:{
        padding:20
    }

});
export default Footer;
