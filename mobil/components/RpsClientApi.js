import React, {useContext} from 'react';
import {GameContext} from "./Contexts";

const RpsClientApi = {

    getNewTokenId :  (setTokenId) => {
        return fetch(`http://localhost:8080/auth/token`,{
            headers : {
                mode: 'cors',
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            }
        }).then(res=>res.text()).then(setTokenId);
    },

    startGame:(tokenId) => {
        return fetch('http://localhost:8080/games/start', {
            headers: {
                token: tokenId,
            }
        }).then(response =>response.json());
    },

    setName:(tokenId,name) => {
        return fetch('http://localhost:8080/user/name',{
            method: 'POST',
            headers: {
                token:tokenId,
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ name:name }),
        }).then(response => response.text());
    },

    openGames:(tokenId,setOpenGames) => {
        return fetch('http://localhost:8080/games',{
            method: "GET",
            headers: {
                token:tokenId,
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
        }).then(response =>response.json()).then(json=>setOpenGames(json));
    },

    getGameStatus:(tokenId,setGame)=>{
        return fetch("http://localhost:8080/games/status",
            {
                method: "GET",
                headers: {
                    Accept: "*/*",
                    "token": tokenId
                }
            })
            .then(res => res.json())
            .then(json => setGame(json))
            .catch(error => console.log(error))
    },

    joinerGame : (gameId, joinerTokenId,setJoinedGame)=>{
        return fetch(`http://localhost:8080/games/join/${gameId}`, {
            method :'GET',
            headers : {
                token:joinerTokenId,
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            }}).then(res=>res.json()).then(json=>setJoinedGame(json));
    },

    searchGames:(status,setSearchOpenGames) => {
        return fetch( `http://localhost:8080/games/search/${status}`)
            .then(response => response.json()).then(json=>setSearchOpenGames(json));
    },

    ownerName:()=>{
        const [game,setGame]=useContext(GameContext);
        return !!game? game.ownerName : "No game";
    },

    ownerTokenId:()=>{
        const [game,setGame]=useContext(GameContext);
        return !!game? game.ownerTokenId : "No game";
    },

    makeMove:(tokenId, sign) => {
        return fetch(`http://localhost:8080/games/move/${sign}`, {
            headers: {
                token: tokenId,
            }
        }).then(response => response.json());
    },

    gameId:() => {
        const [game,setGame]=useContext(GameContext);
        return !!game? game.id : "No game";
    },

    opponentName:() => {
        const [game,setGame]=useContext(GameContext);
        return !!game? game.opponentName : "No game";
    },

    opponentTokenId:()=>{
        const [game,setGame]=useContext(GameContext);
        return !!game? game.opponentTokenId : "No game";
    },

    gameStatus:() => {
        const [game,setGame]=useContext(GameContext);
        return !!game? game.gameStatus : "No game";
    }
}

export default RpsClientApi;