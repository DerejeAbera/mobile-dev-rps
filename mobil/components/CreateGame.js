import React, {useContext, useState} from 'react';
import {Button} from "react-native-web";
import { StyleSheet, Text, View, TouchableOpacity, TextInput, ScrollView } from 'react-native';
import {TokenIdContext} from "./Contexts";
import RpsClientApi from "./RpsClientApi";


const CreateGame = ({navigation}) => {
    const [name, setNameChange] = useState('');
    const [playerName, setPlayerName]=useState('');
    const [game, setGame] = useState([]);

    const postName = () => {
        setTimeout( ()=>{
            setPlayerName(RpsClientApi.setName(tokenId,name).finally(() => finishLoading(false)));
        }, 3000)
        // setInterval
        setTimeout( ()=>{
            setGame(RpsClientApi.startGame(tokenId).then(navigation.navigate("Result")).finally(() => trySubmit(true)));
        }, 10)
        setNameChange('')
    };

    const tokenId = useContext(TokenIdContext);
    return (
        <View style={styles.container}>
            <ScrollView>
                <Text style={styles.text}>{`\n To create a Game.\n`}</Text>
                <Text>tokenId : {tokenId}</Text>

                <TextInput
                    style={styles.textBox}
                    placeholder="Enter Name (Optional)"
                    onChangeText={text => setNameChange(text)}
                    value={name}
                />
                <TouchableOpacity style={styles.button} >
                    <Text>{name}</Text>
                    <Button
                        title="Create Game"
                        onPress={() => postName()}
                    />
                </TouchableOpacity>
            </ScrollView>
        </View>
    );
}

const styles=StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:'#abc',
        alignItems:'center',
        justifyContent:'center',
    },
    button:{
        padding:20,
        width:200,
        height: 150,
    },

    text:{
        fontFamily: 'arial',
        fontSize:20,

    },
    textBox:{
        width:250,
        height: 30,
        padding: 2,
        backgroundColor: '#fff'
    },
    heading: {
        fontFamily: 'OpenSans',
        fontWeight: 'bold',
        paddingTop: 5
    },
});

export  default CreateGame;