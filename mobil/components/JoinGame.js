import React, {useContext, useEffect, useState} from 'react';
import { StyleSheet, Text, View, TouchableOpacity, TextInput, FlatList } from 'react-native';
import {Button} from "react-native-web";
import RpsClientApi from "./RpsClientApi";
import {TokenIdContext} from "./Contexts";


const JoinGame = ({navigation}) => {

    const [dataLoading, finishLoading] = useState(true);
    const [openGames, setOpenGames] = useState([]);
    const [joinerTokenId, setJoinerTokenId] = useState('');
    const [joinerName, setJoinerName]=useState('');
    const [joinedGame, setJoinedGame] = useState(null);
    const [joinerNameChange, setJoinerNameChange] = useState('');
    const [searchOpenGames, setSearchOpenGames] = useState([]);
    const tokenId = useContext(TokenIdContext);
    const status='OPEN';

    useEffect(() =>{
        RpsClientApi.getNewTokenId(setJoinerTokenId)
            .catch((error) => console.error(error))
            .finally(() => finishLoading(false));
    }, []);

    //find the gameId through token
    useEffect(() => {
        setTimeout( ()=>{
            RpsClientApi.openGames(tokenId,setOpenGames)
                .catch((error) => console.error(error))
                .finally(() => finishLoading(false));
        }, 3000)
    },[]);

    useEffect(() =>{
        setTimeout(function (){
            RpsClientApi.searchGames(status,setSearchOpenGames).catch((error) => console.error(error));
        }, 3000)
    },[]);

    const joinGame = (openGameId) => {
        setTimeout( ()=>{
            RpsClientApi.setName(joinerTokenId,joinerNameChange).then(setJoinerName).catch(err=>console.log(err)).finally(()=>finishLoading(false))
        }, 3000)
        RpsClientApi.joinerGame(openGameId,joinerTokenId,setJoinedGame).catch(err=>console.log(err)).finally(()=>finishLoading(false))
        navigation.navigate("PlayGame")
    }

    return(
        <View style={styles.container}>
            <Text>tokenId : {joinerTokenId}</Text>
            <Text style={styles.text}>{`\n To join open games.\n`}</Text>
            <TextInput
                style={styles.textBox}
                placeholder="Enter Name (Optional)"
                onChangeText={text => setJoinerNameChange(text)}
                value={joinerNameChange}
            />
            <Text>{joinerNameChange}</Text>

            <TouchableOpacity>
                <FlatList data={openGames}
                          keyExtractor={({id, index})=>id}
                          renderItem={({item})=>(
                              <Text>gameId={item.id} , name={item.ownerName}, ownerToken= {item.ownerTokenId}
                                  <Button
                                      title="Join Game"
                                      onPress={()=>joinGame(item.id)}
                                  />
                              </Text>
                          )}
                />
            </TouchableOpacity>

        </View>
    );
}
const styles=StyleSheet.create({
    container:{
        flex:1,
        backgroundColor:'#abc',
        alignItems:'center',
        justifyContent:'center',
    },
    button:{
        padding:20,
        width:200,
        height: 150,
    },

    text:{
        fontFamily: 'OpenSans',
        fontSize:20,

    },
    textBox:{
        width:250,
        height: 30,
        padding: 2,
        backgroundColor: '#fff'
    },
    heading: {
        fontFamily: 'OpenSans',
        fontWeight: 'bold',
        paddingTop: 5
    },


});

export default JoinGame;

