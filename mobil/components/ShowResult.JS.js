import React from 'react';
import {StyleSheet, Text, View} from "react-native";
import {Button} from "react-native-web";
import RpsClientApi from "./RpsClientApi";


const ShowResult = ({navigation}) =>{

    return (
        <View>
            <Text>CONGRATULATIONS</Text>
            <Text> Owner {RpsClientApi.gameStatus()} Opponents </Text>
            <Button
                onPress={() => navigation.navigate('PlayGame')}
                title="   Play Again"
                style={styles.button}
            >

            </Button>
        </View>
    )
}

const styles=StyleSheet.create({
    button:{
        height: 40,
        width: 40,
    },
});
export default ShowResult;