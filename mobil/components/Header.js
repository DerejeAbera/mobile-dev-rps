import React from "react";
import {StyleSheet, Text, View, Image, TouchableOpacity} from "react-native";
import logo from '../assets/Rock-paper-scissors.svg';
import * as RootNavigation from './RootNavigation';

const Header = () => {
    return (
        <View style={styles.header}>
            <Image source={logo} style={styles.image}/>
            <TouchableOpacity
                style={styles.button}
                onPress={()=>RootNavigation.navigate('CreateGame')}
            >
                <Text>CreateGame</Text>
            </TouchableOpacity>

            <TouchableOpacity
                style={styles.button}
                onPress={()=>RootNavigation.navigate('JoinGame')}
            >
              <Text>JoinGame</Text>
            </TouchableOpacity>


            <TouchableOpacity
                style={styles.button}
                onPress={()=>RootNavigation.navigate('PlayGame')}
            >
              <Text>PlayGame</Text>
            </TouchableOpacity>
        </View>
    );
}


const styles=StyleSheet.create({
    header:{
        width:'100%',
        height:70,
        flexDirection:'row',
        alignItems:'center',
        justifyContent:'center',
    },

    image:{
        width:50,
        height:50
    },
    button:{
        paddingLeft:50,
        paddingRight:0
    }
});

export default Header;