import React from "react";
import { createStackNavigator } from '@react-navigation/stack';
import Header from "./Header";
import CreateGame from "./CreateGame";
import JoinGame from "./JoinGame";
import PlayerGame from "./PlayerGame.JS";

const Stack = createStackNavigator();

const Nav = () => {
    return (
        <Stack.Navigator
            initialRouteName="Landing"
            screnOptions={{
                headerTintColor:'white',
                headerStyle:{backgroundColor:'orange'}
            }}
        >
        <Stack.Screen name="CreateGame" component={CreateGame}
                       options={{ header: () => <Header headerDisplay="Create Game"/> }}
        />

        <Stack.Screen
            name="JoinGame" component={JoinGame}
            options={{ header: () => <Header headerDisplay="Join Game " /> }}
        />

        <Stack.Screen
            name="PlayGame" component={PlayerGame}
            options={{ header: () => <Header headerDisplay="Play Game " /> }}
        />
        </Stack.Navigator>
    );
}

export default Nav;